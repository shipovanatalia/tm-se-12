package ru.shipova.tm.serializer;

import org.eclipse.persistence.jaxb.JAXBContextFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.ISerializer;
import ru.shipova.tm.entity.Domain;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.HashMap;

import static javax.xml.bind.Marshaller.JAXB_FORMATTED_OUTPUT;
import static ru.shipova.tm.constant.DataConstant.FILE_XML;

public class XmlJaxBSerializer implements ISerializer {
    @Nullable
    private Marshaller marshaller;

    @Nullable
    private Unmarshaller unmarshaller;

    @NotNull
    private final File file;

    public XmlJaxBSerializer() {
        try {
            @Nullable final JAXBContext context = JAXBContextFactory.createContext(new Class[]{Domain.class}, new HashMap());
            marshaller = context.createMarshaller();
            unmarshaller = context.createUnmarshaller();
            marshaller.setProperty(JAXB_FORMATTED_OUTPUT, true);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        file = new File(FILE_XML.displayName());
    }

    @Override
    public void serialize(@NotNull final Domain domain) {
        try {
            if (marshaller == null) return;
            marshaller.marshal(domain, file);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    @Override
    @Nullable
    public Domain deserialize() {
        @Nullable Domain domain = null;
        try {
            if (unmarshaller == null) return null;
            domain = (Domain) unmarshaller.unmarshal(file);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return domain;
    }
}
