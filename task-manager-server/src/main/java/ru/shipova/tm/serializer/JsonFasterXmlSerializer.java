package ru.shipova.tm.serializer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.ISerializer;
import ru.shipova.tm.constant.DataConstant;
import ru.shipova.tm.entity.Domain;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

public class JsonFasterXmlSerializer implements ISerializer {
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public void serialize(@NotNull final Domain domain) throws IOException {
        @NotNull final ObjectWriter objectWriter = objectMapper.writerWithDefaultPrettyPrinter();
        @NotNull final String json = objectWriter.writeValueAsString(domain);
        @NotNull final byte[] data = json.getBytes(StandardCharsets.UTF_8);
        @NotNull final File file = new File(DataConstant.FILE_JSON.displayName());
        Files.write(file.toPath(), data);
    }

    @Override
    @Nullable
    public Domain deserialize() throws IOException {
        @NotNull final File file = new File(DataConstant.FILE_JSON.displayName());
        @Nullable final Domain domain = objectMapper.readValue(file, Domain.class);
        return domain;
    }
}
