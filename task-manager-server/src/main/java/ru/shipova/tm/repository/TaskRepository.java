package ru.shipova.tm.repository;

import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.repository.ITaskRepository;
import ru.shipova.tm.comparator.ComparatorByDateOfBegin;
import ru.shipova.tm.comparator.ComparatorByDateOfCreate;
import ru.shipova.tm.comparator.ComparatorByDateOfEnd;
import ru.shipova.tm.comparator.ComparatorByStatus;
import ru.shipova.tm.constant.FieldConst;
import ru.shipova.tm.constant.Status;
import ru.shipova.tm.constant.TypeOfSort;
import ru.shipova.tm.entity.Task;
import ru.shipova.tm.exception.CommandCorruptException;
import ru.shipova.tm.util.DateToSqlDateUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

@Setter
@NoArgsConstructor
public final class TaskRepository implements ITaskRepository {

    private @Nullable Connection connection;

    @Nullable
    private Task fetch(@Nullable final ResultSet row) throws SQLException {
        if (row == null) return null;
        @NotNull final Task task = new Task();
        task.setId(row.getString(FieldConst.ID));
        task.setName(row.getString(FieldConst.NAME));
        task.setDescription(row.getString(FieldConst.DESCRIPTION));
        task.setDateOfCreate(row.getDate(FieldConst.DATE_CREATE));
        task.setDateOfBegin(row.getDate(FieldConst.DATE_BEGIN));
        task.setDateOfEnd(row.getDate(FieldConst.DATE_END));
        task.setProjectId(row.getString(FieldConst.PROJECT_ID));
        task.setUserId(row.getString(FieldConst.USER_ID));
        @NotNull final Status status = Status.getStatus(row.getString(FieldConst.STATUS));
        switch (status) {
            case PLANNED:
                task.setStatus(Status.PLANNED);
                break;
            case READY:
                task.setStatus(Status.IN_PROCESS);
                break;
            case IN_PROCESS:
                task.setStatus(Status.READY);
                break;
            }
        return task;
    }

    @NotNull
    public List<Task> sort(@NotNull final String typeOfSort,
                           @NotNull final List<Task> taskList) throws CommandCorruptException {
        if (TypeOfSort.DATE_OF_CREATE.displayName().equals(typeOfSort.toUpperCase())) {
            @NotNull final Comparator<Task> comparator = new ComparatorByDateOfCreate<>();
            taskList.sort(comparator);
            return taskList;
        }
        if (TypeOfSort.DATE_OF_BEGIN.displayName().equals(typeOfSort.toUpperCase())) {
            @NotNull final Comparator<Task> comparator = new ComparatorByDateOfBegin<>();
            taskList.sort(comparator);
            return taskList;
        }
        if (TypeOfSort.DATE_OF_END.displayName().equals(typeOfSort.toUpperCase())) {
            @NotNull final Comparator<Task> comparator = new ComparatorByDateOfEnd<>();
            taskList.sort(comparator);
            return taskList;
        }
        if (TypeOfSort.STATUS.displayName().equals(typeOfSort.toUpperCase())) {
            @NotNull final Comparator<Task> comparator = new ComparatorByStatus<Task>();
            taskList.sort(comparator);
            return taskList;
        }
        if (TypeOfSort.NO.displayName().equals(typeOfSort.toUpperCase())) {
            return taskList;
        } else throw new CommandCorruptException();
    }

    @Override
    public void persist(@NotNull final Task task) throws SQLException, ParseException {
        task.setStatus(Status.PLANNED);
        task.setDateOfCreate(new Date());
        @NotNull final String query = "INSERT INTO task (" +
                FieldConst.ID + ", " +
                FieldConst.STATUS + ", " +
                FieldConst.DATE_CREATE + ", " +
                FieldConst.DESCRIPTION + ", " +
                FieldConst.NAME + ", " +
                FieldConst.PROJECT_ID + ", " +
                FieldConst.USER_ID + ") " +
                "VALUES (?, ?, ?, ?, ?, ?, ?)";
        if (connection == null) return;
        @NotNull final PreparedStatement statement;
        statement = connection.prepareStatement(query);
        statement.setString(1, task.getId());
        statement.setObject(2, task.getStatus().displayName());
        statement.setObject(3, DateToSqlDateUtil.formatDate(task.getDateOfCreate()));
        statement.setObject(4, task.getDescription());
        statement.setString(5, task.getName());
        statement.setString(6, task.getProjectId());
        statement.setString(7, task.getUserId());
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void merge(@NotNull final Task task) throws SQLException, ParseException {
        if (findByTaskId(task.getId()) == null) persist(task);
        @NotNull final String query = "UPDATE task SET " +
                FieldConst.DATE_BEGIN + " = ?, " +
                FieldConst.DATE_END + " = ?, " +
                FieldConst.STATUS + " = ?, " +
                FieldConst.DESCRIPTION + " = ?, " +
                FieldConst.NAME + " = ?, " +
                FieldConst.PROJECT_ID + ", " +
                FieldConst.USER_ID + " = ? " +
                "WHERE " + FieldConst.ID + " = ?";
        if (connection == null) return;
        @NotNull final PreparedStatement statement;
        statement = connection.prepareStatement(query);
        statement.setObject(1, DateToSqlDateUtil.formatDate(task.getDateOfBegin()));
        statement.setObject(2, DateToSqlDateUtil.formatDate(task.getDateOfEnd()));
        statement.setString(3, task.getStatus().displayName());
        statement.setString(4, task.getDescription());
        statement.setString(5, task.getName());
        statement.setString(6, task.getProjectId());
        statement.setString(7, task.getUserId());
        statement.setString(8, task.getId());
        statement.executeUpdate();
        statement.close();
    }

    @Nullable
    @Override
    public Task findByTaskId(@NotNull final String taskId) throws SQLException {
        @NotNull final String query = "SELECT * FROM task WHERE " + FieldConst.ID + " = ?";
        if (connection == null) return null;
        @NotNull final PreparedStatement statement;
        statement = connection.prepareStatement(query);
        statement.setString(1, taskId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return null;
        @Nullable final Task task = fetch(resultSet);
        if (task != null) return task;
        statement.close();
        return null;
    }

    @NotNull
    @Override
    public List<Task> findAllByUserId(@NotNull final String userId) throws SQLException {
        @NotNull final List<Task> taskList = new ArrayList<>();
        @NotNull final String query =
                "SELECT * FROM task WHERE user_id = ?";
        if (connection == null) return taskList;
        @NotNull final PreparedStatement statement;
        statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) taskList.add(fetch(resultSet));
        statement.close();
        return taskList;
    }

    @Nullable
    @Override
    public String getTaskIdByName(@NotNull final String taskName) throws SQLException {
        @NotNull final String query = "SELECT * FROM task WHERE " + FieldConst.NAME + " = ?";
        if (connection == null) return null;
        @NotNull final PreparedStatement statement;
        statement = connection.prepareStatement(query);
        statement.setString(1, taskName);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return null;
        @Nullable final Task task = fetch(resultSet);
        if (task != null) return task.getId();
        statement.close();
        return null;
    }

    @NotNull
    @Override
    public List<Task> showAllTasksOfProject(@NotNull final String projectId) throws SQLException {
        @NotNull final List<Task> taskList = new ArrayList<>();
        @NotNull final String query =
                "SELECT * FROM task WHERE project_id = ?";
        if (connection == null) return taskList;
        @NotNull final PreparedStatement statement;
        statement = connection.prepareStatement(query);
        statement.setString(1, projectId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) taskList.add(fetch(resultSet));
        statement.close();
        return taskList;
    }

    @Nullable
    @Override
    public List<Task> search(@Nullable final String userId, @NotNull final String partOfData) throws SQLException {
        @NotNull final String query = "SELECT * FROM task WHERE " +
                FieldConst.USER_ID + " = ? AND " +
                "(" + FieldConst.NAME + " LIKE '%" + partOfData + "%' OR " +
                FieldConst.DESCRIPTION + " LIKE '%" + partOfData + "%')";
        if (connection == null) return null;
        @NotNull final PreparedStatement statement;
        @NotNull final List<Task> taskList = new ArrayList<>();
        statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) {
            taskList.add(fetch(resultSet));
        }
        statement.close();
        return taskList;
    }

    @Override
    public void removeByTaskId(@NotNull String taskId) throws SQLException {
        @NotNull final String query = "DELETE FROM task WHERE " + FieldConst.ID + " = '" + taskId + "'";
        if (connection == null) return;
        @NotNull final PreparedStatement statement;
        statement = connection.prepareStatement(query);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void removeAllByUserId(@NotNull final String userId) throws SQLException {
        @NotNull final String query = "DELETE FROM task WHERE " + FieldConst.USER_ID + " = '" + userId + "'";
        if (connection == null) return;
        @NotNull final PreparedStatement statement;
        statement = connection.prepareStatement(query);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void update(@NotNull final Task task) throws SQLException, ParseException {
        merge(task);
    }

    @Override
    public void removeAllTasksOfProject(@NotNull final String projectId) throws SQLException {
        @NotNull final String query = "DELETE FROM task WHERE " + FieldConst.PROJECT_ID + " = '" + projectId + "'";
        if (connection == null) return;
        @NotNull final PreparedStatement statement;
        statement = connection.prepareStatement(query);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void load(@NotNull final List<Task> taskList) throws SQLException, ParseException {
        for (@Nullable final Task task : taskList) {
            if (task == null) return;
            merge(task);
        }
    }
}
