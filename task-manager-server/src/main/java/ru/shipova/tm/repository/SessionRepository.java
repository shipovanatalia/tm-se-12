package ru.shipova.tm.repository;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.IServiceLocator;
import ru.shipova.tm.api.repository.ISessionRepository;
import ru.shipova.tm.constant.FieldConst;
import ru.shipova.tm.dto.Result;
import ru.shipova.tm.entity.Session;
import ru.shipova.tm.entity.User;
import ru.shipova.tm.util.SignatureUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.UUID;

@Setter
public class SessionRepository implements ISessionRepository {

    private @Nullable IServiceLocator serviceLocator;
    private @Nullable Connection connection;

    public SessionRepository(@Nullable IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Nullable
    private Session fetch(@Nullable final ResultSet row) throws SQLException {
        if (row == null) return null;
        @NotNull final Session session = new Session();
        session.setId(row.getString(FieldConst.ID));
        session.setSignature(row.getString(FieldConst.SIGNATURE));
        session.setTimestamp(row.getLong(FieldConst.TIMESTAMP));
        session.setUserId(row.getString(FieldConst.USER_ID));
        return session;
    }

    @Override
    public void persist(@NotNull final Session session) throws SQLException {
        if (session.getTimestamp() == null) return;

        @NotNull final String query = "INSERT INTO session (" +
                FieldConst.ID + ", " +
                FieldConst.SIGNATURE + ", " +
                FieldConst.TIMESTAMP + ", " +
                FieldConst.USER_ID + ") " +
                "VALUES (?, ?, ?, ?)";

        if (connection == null) return;
        @NotNull final PreparedStatement statement;
        statement = connection.prepareStatement(query);
        statement.setString(1, session.getId());
        statement.setObject(2, session.getSignature());
        statement.setLong(3, session.getTimestamp());
        statement.setObject(4, session.getUserId());
        statement.executeUpdate();
        statement.close();
    }

    @Nullable
    public Session open(@NotNull final String login, @NotNull final String password) throws SQLException {
        if (serviceLocator == null) return null;
        @Nullable final User user = serviceLocator.getIUserService().authorize(login, password);
        if (user == null) return null;
        @NotNull final Session session = new Session();
        session.setId(UUID.randomUUID().toString());
        session.setUserId(user.getId());
        session.setTimestamp(new Date().getTime());
        @NotNull final String salt = "SomeVerySpicySalt";
        @Nullable final String signature = SignatureUtil.sign(session, salt, 100);
        if (signature == null || signature.isEmpty()) return null;
        session.setSignature(signature);
        persist(session);
        return session;
    }

    @Nullable
    public Result close(@NotNull final Session session) throws SQLException {
        removeBySessionId(session.getId());
        return new Result();
    }

    @Override
    public boolean contains(@NotNull String sessionId) throws SQLException {
        @Nullable final Session session = findBySessionId(sessionId);
        return session != null;
    }

    @Override
    public void removeBySessionId(@NotNull final String sessionId) throws SQLException {
        @NotNull final String query = "DELETE FROM session WHERE " + FieldConst.ID + " = '" + sessionId + "'";

        if (connection == null) return;
        @NotNull final PreparedStatement statement;
        statement = connection.prepareStatement(query);
        statement.executeUpdate();
        statement.close();
    }

    @Nullable
    @Override
    public Session findBySessionId(@NotNull final String sessionId) throws SQLException {
        @NotNull final String query = "SELECT * FROM session WHERE " + FieldConst.ID + " = ?";
        if (connection == null) return null;
        @NotNull final PreparedStatement statement;
        statement = connection.prepareStatement(query);
        statement.setString(1, sessionId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return null;
        @Nullable final Session session = fetch(resultSet);
        if (session != null) return session;
        statement.close();
        return null;
    }
}
