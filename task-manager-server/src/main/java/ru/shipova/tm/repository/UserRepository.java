package ru.shipova.tm.repository;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.repository.IUserRepository;
import ru.shipova.tm.constant.FieldConst;
import ru.shipova.tm.constant.RoleType;
import ru.shipova.tm.entity.User;
import ru.shipova.tm.exception.LoginAlreadyExistsException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static ru.shipova.tm.constant.RoleType.ADMIN;
import static ru.shipova.tm.constant.RoleType.USER;

@Getter
@Setter
@NoArgsConstructor
public final class UserRepository implements IUserRepository {

    private @Nullable Connection connection;

    @Nullable
    private User fetch(@Nullable final ResultSet row) throws SQLException {
        if (row == null) return null;
        @NotNull final User user = new User();
        user.setId(row.getString(FieldConst.ID));
        user.setLogin(row.getString(FieldConst.LOGIN));
        user.setPasswordHash(row.getString(FieldConst.PASSWORD_HASH));
        @Nullable final String roleType = row.getString(FieldConst.ROLE_TYPE);
        if (roleType == null) return null;
        switch (roleType.toUpperCase()) {
            case ("АДМИНИСТРАТОР"):
                user.setRoleType(ADMIN);
                break;
            case ("ОБЫЧНЫЙ ПОЛЬЗОВАТЕЛЬ"):
                user.setRoleType(USER);
                break;
        }
        return user;
    }

    @Override
    public void persist(@NotNull final User user) throws LoginAlreadyExistsException, SQLException {
        if (user.getLogin() == null) return;
        if (user.getRoleType() == null) return;
        if (findByLogin(user.getLogin()) != null) throw new LoginAlreadyExistsException();
        @NotNull final String query = "INSERT INTO user (" +
                FieldConst.ID + ", " +
                FieldConst.LOGIN + ", " +
                FieldConst.PASSWORD_HASH + ", " +
                FieldConst.ROLE_TYPE + ") " +
                "VALUES (?, ?, ?, ?)";
        if (connection == null) return;
        @NotNull final PreparedStatement statement;
        statement = connection.prepareStatement(query);
        statement.setString(1, user.getId());
        statement.setObject(2, user.getLogin());
        statement.setString(3, user.getPasswordHash());
        statement.setObject(4, user.getRoleType().displayName());
        statement.executeUpdate();
        statement.close();
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) throws SQLException {
        @NotNull final String query = "SELECT id, login, passwordHash, role FROM user  WHERE " + FieldConst.LOGIN + " = ?";
        if (connection == null) return null;
        @NotNull final PreparedStatement statement;
        statement = connection.prepareStatement(query);
        statement.setString(1, login);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return null;
        @Nullable User user = fetch(resultSet);
        if (user != null) return user;
        statement.close();
        return null;
    }

    @Override
    @Nullable
    public User findById(@NotNull final String userId) throws SQLException {
        @NotNull final String query = "SELECT * FROM user WHERE " + FieldConst.ID + " = ?";
        if (connection == null) return null;
        @NotNull final PreparedStatement statement;
        statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return null;
        @Nullable final User user = fetch(resultSet);
        if (user != null) return user;
        statement.close();
        return null;
    }

    @Override
    public void setNewPassword(@Nullable final String login, @Nullable final String passwordHash) throws SQLException, LoginAlreadyExistsException {
        if (login == null || login.isEmpty()) return;
        if (passwordHash == null || passwordHash.isEmpty()) return;
        @Nullable final User user = findByLogin(login);
        if (user == null) return;
        user.setPasswordHash(passwordHash);
        merge(user);
    }

    @Override
    public void updateUser(@Nullable final String login, @Nullable final RoleType roleType) throws SQLException, LoginAlreadyExistsException {
        if (login == null || login.isEmpty()) return;
        if (roleType == null) return;
        @Nullable final User user = findByLogin(login);
        if (user == null) return;
        user.setRoleType(roleType);
        merge(user);
    }

    @Override
    public void merge(@NotNull final User user) throws SQLException, LoginAlreadyExistsException {
        if (user.getRoleType() == null) return;
        if (findById(user.getId()) == null) persist(user);
        @NotNull final String query = "UPDATE user SET " +
                FieldConst.LOGIN + " = ?, " +
                FieldConst.PASSWORD_HASH + " = ?, " +
                FieldConst.ROLE_TYPE + " = ? " +
                "WHERE " + FieldConst.ID + " = ?";
        if (connection == null) return;
        @NotNull final PreparedStatement statement;
        statement = connection.prepareStatement(query);
        statement.setObject(1, user.getLogin());
        statement.setString(2, user.getPasswordHash());
        statement.setString(3, user.getRoleType().displayName());
        statement.setString(4, user.getId());
        statement.executeUpdate();
        statement.close();
    }

    @NotNull
    @Override
    public List<User> findAll() throws SQLException {
        @NotNull final String query = "SELECT * FROM user";
        @NotNull final List<User> userList = new ArrayList<>();
        if (connection == null) return userList;
        @NotNull final PreparedStatement statement;
        statement = connection.prepareStatement(query);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) {
            userList.add(fetch(resultSet));
        }
        statement.close();
        return userList;
    }

    @Override
    public void load(@NotNull final List<User> userList) throws SQLException, LoginAlreadyExistsException {
        for (@Nullable final User user : userList) {
            if (user == null) return;
            merge(user);
        }
    }
}
