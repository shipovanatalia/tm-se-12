package ru.shipova.tm.repository;

import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.repository.IProjectRepository;
import ru.shipova.tm.comparator.ComparatorByDateOfBegin;
import ru.shipova.tm.comparator.ComparatorByDateOfCreate;
import ru.shipova.tm.comparator.ComparatorByDateOfEnd;
import ru.shipova.tm.comparator.ComparatorByStatus;
import ru.shipova.tm.constant.FieldConst;
import ru.shipova.tm.constant.Status;
import ru.shipova.tm.constant.TypeOfSort;
import ru.shipova.tm.entity.Project;
import ru.shipova.tm.exception.CommandCorruptException;
import ru.shipova.tm.util.DateToSqlDateUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

@Setter
@NoArgsConstructor
public final class ProjectRepository implements IProjectRepository {

    private @Nullable Connection connection;

    @Nullable
    private Project fetch(@Nullable final ResultSet row) throws SQLException {
        if (row == null) return null;
        @NotNull final Project project = new Project();
        project.setId(row.getString(FieldConst.ID));
        project.setName(row.getString(FieldConst.NAME));
        project.setDescription(row.getString(FieldConst.DESCRIPTION));
        project.setDateOfCreate(row.getDate(FieldConst.DATE_CREATE));
        project.setDateOfBegin(row.getDate(FieldConst.DATE_BEGIN));
        project.setDateOfEnd(row.getDate(FieldConst.DATE_END));
        project.setUserId(row.getString(FieldConst.USER_ID));
        @NotNull final Status status = Status.getStatus(row.getString(FieldConst.STATUS));
        switch (status) {
            case PLANNED:
                project.setStatus(Status.PLANNED);
                break;
            case READY:
                project.setStatus(Status.IN_PROCESS);
                break;
            case IN_PROCESS:
                project.setStatus(Status.READY);
                break;
        }
        return project;
    }

    @NotNull
    public List<Project> getProjectListByUserId(@NotNull final String userId) throws SQLException {
        @NotNull final List<Project> projectList = new ArrayList<>();
        @NotNull final String query =
                "SELECT * FROM project WHERE user_id = ?";
        if (connection == null) return projectList;
        @NotNull final PreparedStatement statement;
        statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) projectList.add(fetch(resultSet));
        statement.close();
        return projectList;
    }

    @Override
    public void persist(@NotNull final Project project) throws SQLException, ParseException {
        project.setStatus(Status.PLANNED);
        project.setDateOfCreate(new Date());
        @NotNull final String query = "INSERT INTO project (" +
                FieldConst.ID + ", " +
                FieldConst.STATUS + ", " +
                FieldConst.DATE_CREATE + ", " +
                FieldConst.DESCRIPTION + ", " +
                FieldConst.NAME + ", " +
                FieldConst.USER_ID + ") " +
                "VALUES (?, ?, ?, ?, ?, ?)";
        if (connection == null) return;
        @NotNull final PreparedStatement statement;
        statement = connection.prepareStatement(query);
        statement.setString(1, project.getId());
        statement.setObject(2, project.getStatus().displayName());
        statement.setObject(3, DateToSqlDateUtil.formatDate(project.getDateOfCreate()));
        statement.setObject(4, project.getDescription());
        statement.setString(5, project.getName());
        statement.setString(6, project.getUserId());
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void update(@NotNull final Project project) throws SQLException, ParseException {
        merge(project);
    }

    @Override
    public void merge(@NotNull final Project project) throws SQLException, ParseException {
        if (findByProjectId(project.getId()) == null) persist(project);

        @NotNull final String query = "UPDATE project SET " +
                FieldConst.DATE_BEGIN + " = ?, " +
                FieldConst.DATE_END + " = ?, " +
                FieldConst.STATUS + " = ?, " +
                FieldConst.DESCRIPTION + " = ?, " +
                FieldConst.NAME + " = ?, " +
                FieldConst.USER_ID + " = ? " +
                "WHERE " + FieldConst.ID + " = ?";

        if (connection == null) return;
        @NotNull final PreparedStatement statement;
        statement = connection.prepareStatement(query);
        statement.setObject(1, DateToSqlDateUtil.formatDate(project.getDateOfBegin()));
        statement.setObject(2, DateToSqlDateUtil.formatDate(project.getDateOfEnd()));
        statement.setString(3, project.getStatus().displayName());
        statement.setString(4, project.getDescription());
        statement.setString(5, project.getName());
        statement.setString(6, project.getUserId());
        statement.setString(7, project.getId());
        statement.executeUpdate();
        statement.close();
    }

    @Nullable
    @Override
    public Project findByProjectId(@NotNull final String projectId) throws SQLException {
        @NotNull final String query = "SELECT * FROM project WHERE " + FieldConst.ID + " = ?";

        if (connection == null) return null;
        @NotNull final PreparedStatement statement;
        statement = connection.prepareStatement(query);
        statement.setString(1, projectId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return null;
        @Nullable final Project project = fetch(resultSet);
        if (project != null) return project;
        statement.close();
        return null;
    }

    @Nullable
    @Override
    public String getProjectIdByName(@NotNull final String projectName) throws SQLException {
        @NotNull final String query = "SELECT * FROM project WHERE " + FieldConst.NAME + " = ?";

        if (connection == null) return null;
        @NotNull final PreparedStatement statement;
        statement = connection.prepareStatement(query);
        statement.setString(1, projectName);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return null;
        @Nullable final Project project = fetch(resultSet);
        if (project != null) return project.getId();
        statement.close();
        return null;
    }

    @Nullable
    public List<Project> search(@Nullable final String userId, @NotNull final String partOfData) throws SQLException {
        @NotNull final String query = "SELECT * FROM project WHERE " +
                FieldConst.USER_ID + " = ? AND " +
                "(" + FieldConst.NAME + " LIKE '%" + partOfData + "%' OR " +
                FieldConst.DESCRIPTION + " LIKE '%" + partOfData + "%')";

        if (connection == null) return null;
        @NotNull final PreparedStatement statement;
        @NotNull final List<Project> projectList = new ArrayList<>();
        statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) {
            projectList.add(fetch(resultSet));
        }
        statement.close();
        return projectList;
    }

    @Override
    public void removeAllByUserId(@NotNull final String userId) throws SQLException {
        @NotNull final String query = "DELETE FROM project WHERE " + FieldConst.USER_ID + " = '" + userId + "'";

        if (connection == null) return;
        @NotNull final PreparedStatement statement;
        statement = connection.prepareStatement(query);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void removeByProjectId(@NotNull final String projectId) throws SQLException {
        @NotNull final String query = "DELETE FROM project WHERE " + FieldConst.ID + " = '" + projectId + "'";

        if (connection == null) return;
        @NotNull final PreparedStatement statement;
        statement = connection.prepareStatement(query);
        statement.executeUpdate();
        statement.close();
    }

    @NotNull
    public List<Project> sort(@NotNull final String typeOfSort,
                              @NotNull final List<Project> projectList) throws CommandCorruptException {
        if (TypeOfSort.NO.displayName().equals(typeOfSort.toUpperCase())) {
            return projectList;
        }
        if (TypeOfSort.STATUS.displayName().equals(typeOfSort.toUpperCase())) {
            @NotNull final Comparator<Project> comparator = new ComparatorByStatus<Project>();
            projectList.sort(comparator);
            return projectList;
        }
        if (TypeOfSort.DATE_OF_CREATE.displayName().equals(typeOfSort.toUpperCase())) {
            @NotNull final Comparator<Project> comparator = new ComparatorByDateOfCreate<>();
            projectList.sort(comparator);
            return projectList;
        }
        if (TypeOfSort.DATE_OF_BEGIN.displayName().equals(typeOfSort.toUpperCase())) {
            @NotNull final Comparator<Project> comparator = new ComparatorByDateOfBegin<>();
            projectList.sort(comparator);
            return projectList;
        }
        if (TypeOfSort.DATE_OF_END.displayName().equals(typeOfSort.toUpperCase())) {
            @NotNull final Comparator<Project> comparator = new ComparatorByDateOfEnd<>();
            projectList.sort(comparator);
            return projectList;
        } else throw new CommandCorruptException();
    }

    public void load(@NotNull final List<Project> projectList) throws SQLException, ParseException {
        for (@Nullable final Project project : projectList) {
            if (project == null) return;
            merge(project);
        }
    }
}
