package ru.shipova.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.shipova.tm.api.repository.IProjectRepository;
import ru.shipova.tm.api.repository.ITaskRepository;
import ru.shipova.tm.api.service.IProjectService;
import ru.shipova.tm.entity.Project;
import ru.shipova.tm.exception.CommandCorruptException;
import ru.shipova.tm.util.DataBaseUtil;

import javax.annotation.Nullable;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public final class ProjectService implements IProjectService {
    @NotNull private final IProjectRepository projectRepository;
    @NotNull private final ITaskRepository taskRepository;

    private @Nullable Connection connection;

    public ProjectService(@NotNull IProjectRepository projectRepository, @NotNull ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
        try {
            this.connection = DataBaseUtil.connect();
        } catch (Exception e) {
            System.out.println("OOPS, YOU HAVE A PROBLEM WITH CONNECTION TO DATA BASE.");
        }
        this.projectRepository.setConnection(connection);
        this.taskRepository.setConnection(connection);
    }

    @Override
    @Nullable
    public List<Project> getProjectListOfUser(final @Nullable String userId) {
        if (userId == null || userId.isEmpty()) return null;
        try {
            return projectRepository.getProjectListByUserId(userId);
        } catch (SQLException e) {
            System.out.println("OOPS, YOU HAVE AN SQL EXCEPTION.");
        }
        return new ArrayList<>();
    }

    @Override
    @Nullable
    public List<Project> getSortedProjectListOfUser(@NotNull final String userId, @NotNull final String typeOfSort) throws CommandCorruptException {
        @Nullable final List<Project> projectList = getProjectListOfUser(userId);
        if (projectList == null) return null;
        return sort(typeOfSort, projectList);
    }

    @Override
    public void load(@Nullable final List<Project> projectList) {
        if (projectList == null) return;
        try {
            projectRepository.load(projectList);
        } catch (SQLException e) {
            System.out.println("OOPS, YOU HAVE AN SQL EXCEPTION.");
        } catch (ParseException e) {
            System.out.println("OOPS, IMPOSSIBLE TO PARSE.");
        }
    }

    @NotNull
    @Override
    public List<Project> sort(@NotNull final String typeOfSort, @NotNull final List<Project> projectList) throws CommandCorruptException {
        return projectRepository.sort(typeOfSort, projectList);
    }

    @Nullable
    @Override
    public List<Project> search(@Nullable final String userId, @Nullable final String partOfData) {
        if (userId == null || userId.isEmpty()) return null;
        if (partOfData == null || partOfData.isEmpty()) return null;
        try {
            return projectRepository.search(userId, partOfData);
        } catch (SQLException e) {
            System.out.println("OOPS, YOU HAVE AN SQL EXCEPTION.");
        }
        return new ArrayList<>();
    }

    @Override
    public void create(@NotNull final String userId, @NotNull final String projectName) {
        @NotNull final String projectId = UUID.randomUUID().toString();
        try {
            projectRepository.persist(new Project(projectId, userId, projectName));
        } catch (SQLException e) {
            System.out.println("OOPS, YOU HAVE AN SQL EXCEPTION.");
        } catch (ParseException e) {
            System.out.println("OOPS, IMPOSSIBLE TO PARSE.");
        }
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        @Nullable final List<Project> projectList;
        try {
            projectList = projectRepository.getProjectListByUserId(userId);
            if (projectList == null) return;
            for (@Nullable final Project project : projectList) {
                if (project == null) return;
                taskRepository.removeAllTasksOfProject(project.getId());
            }
            projectRepository.removeAllByUserId(userId);
        } catch (SQLException e) {
            System.out.println("OOPS, YOU HAVE AN SQL EXCEPTION.");
        }
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String projectName) {
        if (projectName == null || projectName.isEmpty()) return;
        if (userId == null || userId.isEmpty()) return;
        @Nullable final String projectId;
        try {
            projectId = projectRepository.getProjectIdByName(projectName);
            if (projectId == null) return;
            @Nullable final Project project = projectRepository.findByProjectId(projectId);
            if (project == null) return;
            if (!userId.equals(project.getUserId())) return;
            projectRepository.removeByProjectId(projectId);
            taskRepository.removeAllTasksOfProject(projectId);
        } catch (SQLException e) {
            System.out.println("OOPS, YOU HAVE AN SQL EXCEPTION.");
        }
    }
}
