package ru.shipova.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.shipova.tm.api.repository.IProjectRepository;
import ru.shipova.tm.api.repository.ITaskRepository;
import ru.shipova.tm.api.service.ITaskService;
import ru.shipova.tm.entity.Task;
import ru.shipova.tm.exception.CommandCorruptException;
import ru.shipova.tm.exception.ProjectDoesNotExistException;
import ru.shipova.tm.util.DataBaseUtil;

import javax.annotation.Nullable;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public final class TaskService implements ITaskService {
    private @NotNull
    final ITaskRepository taskRepository;

    private @NotNull
    final IProjectRepository projectRepository;

    private @Nullable Connection connection;

    @NotNull
    public TaskService(@NotNull final ITaskRepository taskRepository, @NotNull final IProjectRepository projectRepository) {
        try {
            this.connection = DataBaseUtil.connect();
        } catch (Exception e) {
            System.out.println("OOPS, YOU HAVE A PROBLEM WITH CONNECTION TO DATA BASE.");
        }
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
        this.taskRepository.setConnection(connection);
        this.projectRepository.setConnection(connection);
    }

    @NotNull
    @Override
    public List<Task> showAllTasksOfProject(@NotNull final String projectName) throws ProjectDoesNotExistException {
        if (projectName.isEmpty()) throw new ProjectDoesNotExistException();

        @Nullable final String projectId;
        try {
            projectId = projectRepository.getProjectIdByName(projectName);
            if (projectId == null) return new ArrayList<>();
            if (projectRepository.findByProjectId(projectId) == null) throw new ProjectDoesNotExistException();
            return taskRepository.showAllTasksOfProject(projectId);
        } catch (SQLException e) {
            System.out.println("OOPS, YOU HAVE AN SQL EXCEPTION.");
        }
        return new ArrayList<>();
    }

    @Nullable
    @Override
    public List<Task> getTaskListOfUser(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return null;
        try {
            return taskRepository.findAllByUserId(userId);
        } catch (SQLException e) {
            System.out.println("OOPS, YOU HAVE AN SQL EXCEPTION.");
        }
        return new ArrayList<>();
    }

    @Override
    public void load(@Nullable final List<Task> taskList) {
        if (taskList == null) return;
        try {
            taskRepository.load(taskList);
        } catch (SQLException e) {
            System.out.println("OOPS, YOU HAVE AN SQL EXCEPTION.");
        } catch (ParseException e) {
            System.out.println("OOPS, IMPOSSIBLE TO PARSE.");
        }
    }

    @Override
    @Nullable
    public List<Task> getSortedTaskListOfUser(@NotNull final String userId, @NotNull final String typeOfSort) throws CommandCorruptException {
        @Nullable final List<Task> taskList = getTaskListOfUser(userId);
        if (taskList == null) return null;
        return sort(typeOfSort, taskList);
    }

    @Override
    @NotNull
    public List<Task> sort(@NotNull final String typeOfSort, @NotNull final List<Task> taskList) throws CommandCorruptException {
        return taskRepository.sort(typeOfSort, taskList);
    }

    @Nullable
    @Override
    public List<Task> search(@Nullable final String userId, @Nullable final String partOfData) {
        if (userId == null || userId.isEmpty()) return null;
        if (partOfData == null || partOfData.isEmpty()) return null;
        try {
            return taskRepository.search(userId, partOfData);
        } catch (SQLException e) {
            System.out.println("OOPS, YOU HAVE AN SQL EXCEPTION.");
        }
        return new ArrayList<>();
    }

    @Override
    public void create(@Nullable final String userId, @Nullable final String taskName, @Nullable final String projectName) throws ProjectDoesNotExistException {
        if (userId == null || userId.isEmpty()) return;
        if (taskName == null || taskName.isEmpty()) return;
        if (projectName == null || projectName.isEmpty()) return;

        @Nullable final String projectId;
        try {
            projectId = projectRepository.getProjectIdByName(projectName);
            if (projectId == null) return;
            if (projectRepository.findByProjectId(projectId) == null) throw new ProjectDoesNotExistException();
            @NotNull final String taskId = UUID.randomUUID().toString();
            taskRepository.persist(new Task(taskId, taskName, projectId, userId));
        } catch (SQLException e) {
            System.out.println("OOPS, YOU HAVE AN SQL EXCEPTION.");
        } catch (ParseException e) {
            System.out.println("OOPS, IMPOSSIBLE TO PARSE.");
        }
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        try {
            taskRepository.removeAllByUserId(userId);
        } catch (SQLException e) {
            System.out.println("OOPS, YOU HAVE AN SQL EXCEPTION.");
        }
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String taskName) {
        if (taskName == null || taskName.isEmpty()) return;
        if (userId == null || userId.isEmpty()) return;
        @Nullable final String taskId;
        try {
            taskId = taskRepository.getTaskIdByName(taskName);
            if (taskId == null) return;
            @Nullable final Task task = taskRepository.findByTaskId(taskId);
            if (task == null) return;
            task.setDateOfEnd(new Date());
            if (!userId.equals(task.getUserId())) return;
            taskRepository.removeByTaskId(taskId);
        } catch (SQLException e) {
            System.out.println("OOPS, YOU HAVE AN SQL EXCEPTION.");
        }
    }
}
