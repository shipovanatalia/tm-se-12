package ru.shipova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.IServiceLocator;
import ru.shipova.tm.api.repository.ISessionRepository;
import ru.shipova.tm.api.service.ISessionService;
import ru.shipova.tm.dto.Result;
import ru.shipova.tm.entity.Session;
import ru.shipova.tm.exception.AccessForbiddenException;
import ru.shipova.tm.util.DataBaseUtil;
import ru.shipova.tm.util.SignatureUtil;

import java.sql.Connection;
import java.sql.SQLException;

public class SessionService implements ISessionService {

    private @NotNull final ISessionRepository sessionRepository;
    private @NotNull final IServiceLocator serviceLocator;
    private @Nullable Connection connection;

    public SessionService(@NotNull final ISessionRepository sessionRepository, @NotNull final IServiceLocator serviceLocator) {
        this.sessionRepository = sessionRepository;
        this.serviceLocator = serviceLocator;
        try {
            this.connection = DataBaseUtil.connect();
        } catch (Exception e) {
            System.out.println("OOPS, YOU HAVE A PROBLEM WITH CONNECTION TO DATA BASE.");
        }
        this.sessionRepository.setConnection(connection);
    }

    @Override
    @Nullable
    public Session open(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        try {
            return sessionRepository.open(login, password);
        } catch (SQLException e) {
            System.out.println("OOPS, YOU HAVE AN SQL EXCEPTION.");
        }
        return null;
    }

    @Override
    @Nullable
    public Result close(@NotNull final Session session) {
        try {
            return sessionRepository.close(session);
        } catch (SQLException e) {
            System.out.println("OOPS, YOU HAVE AN SQL EXCEPTION.");
        }
        return null;
    }

    @Override
    public void validate(@Nullable final Session session) throws AccessForbiddenException {
        if (session == null) throw new AccessForbiddenException();
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessForbiddenException();
        if (session.getTimestamp() == null) throw new AccessForbiddenException();
        @Nullable final Session temp = session.clone();
        if (temp == null) throw new AccessForbiddenException();
        @NotNull final String signatureSource = session.getSignature();
        SignatureUtil.sign(temp, "SomeVerySpicySalt", 100);
        @Nullable final String signatureTarget = temp.getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessForbiddenException();
        try {
            if (!sessionRepository.contains(session.getId())) throw new AccessForbiddenException();
        } catch (SQLException e) {
            System.out.println("OOPS, YOU HAVE AN SQL EXCEPTION.");
        }
    }
}
