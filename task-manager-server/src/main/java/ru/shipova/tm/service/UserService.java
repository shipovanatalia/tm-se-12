package ru.shipova.tm.service;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.repository.IUserRepository;
import ru.shipova.tm.api.service.IUserService;
import ru.shipova.tm.constant.RoleType;
import ru.shipova.tm.entity.Session;
import ru.shipova.tm.entity.User;
import ru.shipova.tm.exception.LoginAlreadyExistsException;
import ru.shipova.tm.util.DataBaseUtil;
import ru.shipova.tm.util.HashUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
public final class UserService implements IUserService {

    private @NotNull final IUserRepository userRepository;
    private @Nullable Connection connection;

    @NotNull
    public UserService(@NotNull final IUserRepository userRepository) {
        try {
            this.connection = DataBaseUtil.connect();
        } catch (Exception e) {
            System.out.println("OOPS, YOU HAVE A PROBLEM WITH CONNECTION TO DATA BASE.");
        }
        this.userRepository = userRepository;
        this.userRepository.setConnection(connection);
    }

    @Override
    public void createUser(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final RoleType roleType
    ) throws LoginAlreadyExistsException {
        if (login == null || login.isEmpty()) return;
        if (password == null || password.isEmpty()) return;
        if (roleType == null) return;
        @NotNull final String userId = UUID.randomUUID().toString();
        @Nullable final String passwordHash = HashUtil.md5(password);
        if (passwordHash == null) return;
        final User user = new User(userId, login, passwordHash, roleType);
        try {
            userRepository.persist(user);
        } catch (SQLException e) {
            System.out.println("OOPS, YOU HAVE AN SQL EXCEPTION.");
        }
    }


    @Nullable
    @Override
    public User authorize(@NotNull final String login, @NotNull final String password) {
        final boolean check = checkDataAccess(login, password);
        if (!check) return null;
        try {
            return userRepository.findByLogin(login);
        } catch (SQLException e) {
            System.out.println("OOPS, YOU HAVE AN SQL EXCEPTION.");
        }
        return null;
    }

    @Override
    public boolean checkDataAccess(@NotNull final String login, @NotNull final String password) {
        if (login.isEmpty()) return false;
        if (password.isEmpty()) return false;
        @Nullable final User user = findByLogin(login);
        if (user == null) return false;
        @Nullable final String passwordHash = HashUtil.md5(password);
        if (passwordHash == null) return false;
        return passwordHash.equals(user.getPasswordHash());
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) return null;
        try {
            return userRepository.findByLogin(login);
        } catch (SQLException e) {
            System.out.println("OOPS, YOU HAVE AN SQL EXCEPTION.");
        }
        return null;
    }

    @Nullable
    @Override
    public User findById(@NotNull final String userId) {
        try {
            return userRepository.findById(userId);
        } catch (SQLException e) {
            System.out.println("OOPS, YOU HAVE AN SQL EXCEPTION.");
        }
        return null;
    }

    @Nullable
    @Override
    public RoleType getRoleType(@NotNull final Session session){
        @Nullable User user = findById(session.getUserId());
        if (user == null) return null;
        @Nullable final RoleType roleType = user.getRoleType();
        return roleType;
    }

    @Nullable
    @Override
    public RoleType getRoleType(@Nullable final String role){
        if (role == null || role.isEmpty()) return null;
        RoleType roleType = null;
        switch (role.toUpperCase()) {
            case "USER":
                roleType = RoleType.USER;
                break;
            case "ADMIN":
                roleType = RoleType.ADMIN;
                break;
        }
        return roleType;
    }


    @Override
    public void registryUser(@Nullable final String login, @Nullable final String password, @Nullable final String role) throws LoginAlreadyExistsException {
        if (login == null || login.isEmpty()) return;
        if (password == null || password.isEmpty()) return;
        if (role == null || role.isEmpty()) return;
        @Nullable final String passwordHash = HashUtil.md5(password);
        @NotNull final String userId = UUID.randomUUID().toString();
        @Nullable final RoleType roleType = getRoleType(role);
        try {
            userRepository.persist(new User(userId, login, passwordHash, roleType));
        } catch (SQLException e) {
            System.out.println("OOPS, YOU HAVE AN SQL EXCEPTION.");
        }
    }

    @Override
    public void updateUser(@Nullable final String login, @Nullable final String role){
        if (role == null || role.isEmpty()) return;
        if (login == null || login.isEmpty()) return;
        @Nullable final RoleType roleType = getRoleType(role);
        try {
            userRepository.updateUser(login, roleType);
        } catch (SQLException e) {
            System.out.println("OOPS, YOU HAVE AN SQL EXCEPTION.");
        } catch (LoginAlreadyExistsException e) {
            System.out.println("LOGIN " + login + " ALREADY EXISTS.");
        }
    }

    @Override
    public void setNewPassword(@Nullable final String login, @NotNull final String password){
        if (password.isEmpty()) return;
        if (login == null || login.isEmpty()) return;

        @Nullable final String passwordHash = HashUtil.md5(password);
        try {
            userRepository.setNewPassword(login, passwordHash);
        } catch (SQLException e) {
            System.out.println("OOPS, YOU HAVE AN SQL EXCEPTION.");
        } catch (LoginAlreadyExistsException e) {
            System.out.println("LOGIN " + login + " ALREADY EXISTS.");
        }
    }

    @Override
    @Nullable
    public List<User> getUserList() {
        try {
            return userRepository.findAll();
        } catch (SQLException e) {
            System.out.println("OOPS, YOU HAVE AN SQL EXCEPTION.");
        }
        return new ArrayList<>();
    }

    @Override
    public void load(@Nullable final List<User> userList) {
        if (userList == null) return;
        try {
            userRepository.load(userList);
        } catch (SQLException e) {
            System.out.println("OOPS, YOU HAVE AN SQL EXCEPTION.");
        } catch (LoginAlreadyExistsException e) {
            System.out.println("LOGIN ALREADY EXISTS.");
        }
    }
}
