package ru.shipova.tm.service;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.service.IAdminUserService;
import ru.shipova.tm.api.service.IUserService;
import ru.shipova.tm.constant.RoleType;
import ru.shipova.tm.entity.Session;

@RequiredArgsConstructor
public class AdminUserService implements IAdminUserService {

    private @NotNull
    final IUserService userService;

    @Override
    public boolean getAccessToUsualCommand(@Nullable final Session session, boolean needAuthorize){
        return needAuthorize && session != null;
    }

    @Override
    public boolean getAccessToAdminCommand(@Nullable final Session session, boolean isOnlyAdminCommand){
        if (session == null) return false;
        @NotNull final RoleType userRole = userService.getRoleType(session);
        if (!isOnlyAdminCommand && RoleType.ADMIN.equals(userRole)) return true;
        return isOnlyAdminCommand && RoleType.ADMIN.equals(userRole);
    }
}
