package ru.shipova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.dto.Result;
import ru.shipova.tm.entity.Session;

import java.sql.Connection;
import java.sql.SQLException;

public interface ISessionRepository {
    void setConnection(Connection connection);

    void persist(@NotNull Session session) throws SQLException;

    @Nullable
    Session open(@NotNull final String login, @NotNull final String password) throws SQLException;
    @Nullable
    Result close(@NotNull final Session session) throws SQLException;
    boolean contains(@NotNull final String sessionId) throws SQLException;

    void removeBySessionId(@NotNull String sessionId) throws SQLException;

    @Nullable Session findBySessionId(@NotNull String sessionId) throws SQLException;
}
