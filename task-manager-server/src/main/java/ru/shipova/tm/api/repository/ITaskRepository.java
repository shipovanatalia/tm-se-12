package ru.shipova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.entity.Task;
import ru.shipova.tm.exception.CommandCorruptException;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

public interface ITaskRepository {
    void setConnection(Connection connection);

    @Nullable Task findByTaskId(@NotNull final String taskId) throws SQLException;

    @Nullable List<Task> findAllByUserId(@NotNull final String userId) throws SQLException;

    @NotNull List<Task> sort(@NotNull final String typeOfSort, @NotNull List<Task> taskList) throws CommandCorruptException;

    @Nullable List<Task> search(@Nullable final String userId, @NotNull final String partOfData) throws SQLException;

    @Nullable String getTaskIdByName(@NotNull final String taskName) throws SQLException;

    void persist(@NotNull final Task task) throws SQLException, ParseException;

    void merge(@NotNull final Task task) throws SQLException, ParseException;

    void removeByTaskId(@NotNull final String taskId) throws SQLException;

    void removeAllByUserId(@NotNull final String userId) throws SQLException;

    void update(@NotNull final Task task) throws SQLException, ParseException;

    @NotNull List<Task> showAllTasksOfProject(@NotNull final String projectId) throws SQLException;

    void removeAllTasksOfProject(@NotNull final String projectId) throws SQLException;

    void load(@NotNull final List<Task> taskList) throws SQLException, ParseException;
}
