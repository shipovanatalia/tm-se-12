package ru.shipova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.dto.Result;
import ru.shipova.tm.entity.Session;
import ru.shipova.tm.exception.AccessForbiddenException;


public interface ISessionEndpoint {
    @Nullable
    Session openSession(@NotNull final String login, @NotNull final String password);
    @Nullable
    Result closeSession(@NotNull final Session session) throws AccessForbiddenException;
}
