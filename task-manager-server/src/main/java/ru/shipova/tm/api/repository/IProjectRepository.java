package ru.shipova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.entity.Project;
import ru.shipova.tm.exception.CommandCorruptException;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

public interface IProjectRepository {
    void setConnection(Connection connection);

    @Nullable Project findByProjectId(@NotNull final String projectId) throws SQLException;

    @Nullable List<Project> getProjectListByUserId(@NotNull final String userId) throws SQLException;

    @NotNull List<Project> sort(@NotNull final String typeOfSort, @NotNull List<Project> projectList) throws CommandCorruptException;

    @Nullable String getProjectIdByName(@NotNull final String projectName) throws SQLException;

    @Nullable List<Project> search(@Nullable final String userId, @NotNull final String partOfData) throws SQLException;

    void persist(@NotNull final Project project) throws SQLException, ParseException;

    void merge(@NotNull final Project project) throws SQLException, ParseException;

    void removeByProjectId(@NotNull final String projectId) throws SQLException;

    void removeAllByUserId(@NotNull final String userId) throws SQLException;

    void update(@NotNull final Project project) throws SQLException, ParseException;

    void load(@NotNull final List<Project> projectList) throws SQLException, ParseException;
}
