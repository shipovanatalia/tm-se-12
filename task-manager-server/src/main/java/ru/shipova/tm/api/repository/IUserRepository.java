package ru.shipova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.constant.RoleType;
import ru.shipova.tm.entity.User;
import ru.shipova.tm.exception.LoginAlreadyExistsException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public interface IUserRepository {
    void setConnection(Connection connection);

    @Nullable User findByLogin(@NotNull final String login) throws SQLException;

    @Nullable User findById(@NotNull final String userId) throws SQLException;

    void persist(@NotNull final User user) throws LoginAlreadyExistsException, SQLException;

    void setNewPassword(@Nullable final String login, @Nullable final String passwordHash) throws SQLException, LoginAlreadyExistsException;

    void updateUser(@Nullable final String login, @Nullable final RoleType roleType) throws SQLException, LoginAlreadyExistsException;

    void merge(@NotNull User user) throws SQLException, LoginAlreadyExistsException;

    @Nullable List<User> findAll() throws SQLException;

    void load(@NotNull final List<User> userList) throws SQLException, LoginAlreadyExistsException;
}
