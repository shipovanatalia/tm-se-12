package ru.shipova.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.IServiceLocator;
import ru.shipova.tm.api.endpoint.ISessionEndpoint;
import ru.shipova.tm.api.repository.IProjectRepository;
import ru.shipova.tm.api.repository.ISessionRepository;
import ru.shipova.tm.api.repository.ITaskRepository;
import ru.shipova.tm.api.repository.IUserRepository;
import ru.shipova.tm.api.service.*;
import ru.shipova.tm.constant.RoleType;
import ru.shipova.tm.endpoint.*;
import ru.shipova.tm.entity.User;
import ru.shipova.tm.exception.LoginAlreadyExistsException;
import ru.shipova.tm.repository.ProjectRepository;
import ru.shipova.tm.repository.SessionRepository;
import ru.shipova.tm.repository.TaskRepository;
import ru.shipova.tm.repository.UserRepository;
import ru.shipova.tm.service.*;

import javax.xml.ws.Endpoint;
import java.sql.SQLException;
import java.util.List;

/**
 * Класс загрузчика приложения
 */

public final class Bootstrap implements IServiceLocator {
    @NotNull
    private final PropertyService propertyService = new PropertyService();
    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();
    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository, taskRepository);
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository, projectRepository);
    @NotNull
    private final IUserRepository userRepository = new UserRepository();
    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository(this);
    @NotNull
    private final IUserService userService = new UserService(userRepository);
    @NotNull
    private final IDomainService domainService = new DomainService(this);
    @NotNull
    private final ISessionService sessionService = new SessionService(sessionRepository, this);
    @NotNull
    private final IAdminUserService adminUserService = new AdminUserService(userService);
    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpoint(this);
    @NotNull
    private final ProjectEndpoint projectEndpoint = new ProjectEndpoint(this);
    @NotNull
    private final UserEndpoint userEndpoint = new UserEndpoint(this);
    @NotNull
    private final TaskEndpoint taskEndpoint = new TaskEndpoint(this);
    @NotNull
    private final DomainEndpoint domainEndpoint = new DomainEndpoint(this);
    @NotNull
    private final AdminUserEndpoint adminUserEndpoint = new AdminUserEndpoint(this);

    public void init() {
        initEndpoint();
        @Nullable final List<User> userList;
        try {
            userList = userRepository.findAll();
            if (userList == null || userList.isEmpty()) initDefaultUsers();
        } catch (SQLException e) {
            System.out.println("OOPS, YOU HAVE AN SQL EXCEPTION.");
        }
    }

    private void initEndpoint(){
        registry(sessionEndpoint);
        registry(projectEndpoint);
        registry(domainEndpoint);
        registry(userEndpoint);
        registry(taskEndpoint);
        registry(adminUserEndpoint);
    }

    private void registry(@Nullable final Object endpoint){
        if (endpoint == null) return;
        @NotNull final String host = propertyService.getServerHost();
        @NotNull Integer port = propertyService.getServerPort();
        @NotNull String name = endpoint.getClass().getSimpleName();
        @NotNull String wsdl = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(wsdl, endpoint);
    }

    private void initDefaultUsers(){
        try {
            userService.createUser("user", "user", RoleType.USER);
            userService.createUser("admin", "admin", RoleType.ADMIN);
        } catch (LoginAlreadyExistsException e) {
            e.printStackTrace();
        }
    }

    @NotNull
    @Override
    public IProjectService getIProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public ITaskService getITaskService() {
        return taskService;
    }

    @NotNull
    @Override
    public IUserService getIUserService() {
        return userService;
    }

    @Override
    public @NotNull IDomainService getIDomainService() { return domainService; }

    @Override
    public @NotNull ISessionService getISessionService() {
        return sessionService;
    }

    @Override
    public @NotNull IAdminUserService getIAdminUserService() {
        return adminUserService;
    }
}
