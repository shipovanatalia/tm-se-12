package ru.shipova.tm.constant;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public enum Status {
    PLANNED("ЗАПЛАНИРОВАННО"),
    IN_PROCESS("В ПРОЦЕССЕ"),
    READY("ГОТОВО");

    private @NotNull final String name;

    Status(@NotNull final String name) {
        this.name = name;
    }

    @NotNull public String displayName() {
        return name;
    }

    @NotNull
    public static Status getStatus(@Nullable final String displayName){
        for (@NotNull final Status status: Status.values()) {
            if (status.displayName().equals(displayName))
                return status;
        }
        return PLANNED;
    }
}
