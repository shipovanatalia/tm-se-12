package ru.shipova.tm.util;

import org.jetbrains.annotations.NotNull;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

public final class DataBaseUtil {
    @NotNull
    public static Connection connect() throws Exception {
//        @NotNull final FileInputStream fileInputStream =
//                new FileInputStream("task-manager-server/src/main/resources/db.properties");
        final InputStream inputStream = DataBaseUtil.class.getClassLoader().getResourceAsStream("db.properties");
        final Properties property = new Properties();
        property.load(inputStream);

        final String URL = property.getProperty("db.host");
        final String USER = property.getProperty("db.login");
        final String PASSWORD = property.getProperty("db.password");
        final String driver = property.getProperty("jdbc.driver");
        if (driver != null) {
            Class.forName(driver);
        }
        return DriverManager.getConnection(URL, USER, PASSWORD);
    }
}

