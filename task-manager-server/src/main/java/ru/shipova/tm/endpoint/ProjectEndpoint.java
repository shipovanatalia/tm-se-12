package ru.shipova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.shipova.tm.api.IServiceLocator;
import ru.shipova.tm.entity.Project;
import ru.shipova.tm.entity.Session;
import ru.shipova.tm.exception.AccessForbiddenException;
import ru.shipova.tm.exception.CommandCorruptException;

import javax.annotation.Nullable;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

@WebService(name = "ProjectEndpoint", targetNamespace = "http://endpoint.tm.shipova.ru/")
public class ProjectEndpoint extends AbstractEndpoint {
    public ProjectEndpoint() {
        super(null);
    }

    public ProjectEndpoint(@Nullable final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    public List<Project> getSortedProjectListOfUser(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "typeOfSort", partName = "typeOfSort") @NotNull final String typeOfSort
    ) throws AccessForbiddenException, CommandCorruptException {
        if (serviceLocator == null) return new ArrayList<>();
        serviceLocator.getISessionService().validate(session);
        List<Project> sortedList = serviceLocator.getIProjectService().getSortedProjectListOfUser(session.getUserId(), typeOfSort);
        if (sortedList == null) return new ArrayList<>();
        return sortedList;
    }

//    @Nullable
//    @WebMethod
//    public List<Project> getAllProjectList(
//            @WebParam(name = "session", partName = "session") @NotNull final Session session
//    ) throws AccessForbiddenException {
//        if (serviceLocator == null) return new ArrayList<>();
//        serviceLocator.getISessionService().validate(session);
//        return serviceLocator.getIProjectService().getListProject();
//    }

    @WebMethod
    public void loadProjectList
            (@WebParam(name = "session", partName = "session") @NotNull final Session session,
             @WebParam(name = "projectList", partName = "projectList") @Nullable final List<Project> projectList
            ) throws AccessForbiddenException {
        if (serviceLocator == null) return;
        serviceLocator.getISessionService().validate(session);
        serviceLocator.getIProjectService().load(projectList);
    }

    @Nullable
    @WebMethod
    public List<Project> searchProject(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam (name = "partOfData", partName = "partOfData") @NotNull final String partOfData
    ) throws AccessForbiddenException {
        if (serviceLocator == null) return new ArrayList<>();
        serviceLocator.getISessionService().validate(session);
        return serviceLocator.getIProjectService().search(session.getUserId(), partOfData);
    }

    @WebMethod
    public void createProject(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "projectName", partName = "projectName") @NotNull final String projectName
    ) throws AccessForbiddenException {
        if (serviceLocator == null) return;
        serviceLocator.getISessionService().validate(session);
        serviceLocator.getIProjectService().create(session.getUserId(), projectName);
    }

    @WebMethod
    public void clearProjectListOfUser(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) throws AccessForbiddenException {
        if (serviceLocator == null) return;
        serviceLocator.getISessionService().validate(session);
        serviceLocator.getIProjectService().clear(session.getUserId());
    }

    @WebMethod
    public void removeProject(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "projectName", partName = "projectName") @Nullable final String projectName
    ) throws AccessForbiddenException {
        if (serviceLocator == null) return;
        serviceLocator.getISessionService().validate(session);
        serviceLocator.getIProjectService().remove(session.getUserId(), projectName);
    }
}
