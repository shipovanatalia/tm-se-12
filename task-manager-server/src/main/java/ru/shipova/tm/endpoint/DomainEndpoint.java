package ru.shipova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.api.IServiceLocator;
import ru.shipova.tm.entity.Domain;
import ru.shipova.tm.entity.Session;
import ru.shipova.tm.exception.AccessForbiddenException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.io.IOException;

@WebService(name = "DomainEndpoint", targetNamespace = "http://endpoint.tm.shipova.ru/")
public class DomainEndpoint extends AbstractEndpoint {
    public DomainEndpoint() {
        super(null);
    }

    public DomainEndpoint(@Nullable IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @WebMethod
    public void load(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "domain", partName = "domain") @Nullable final Domain domain
    ) throws AccessForbiddenException {
        if (serviceLocator == null) return;
        serviceLocator.getISessionService().validate(session);
        serviceLocator.getIDomainService().load(domain);
    }

    @WebMethod
    public void serialize(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "domain", partName = "domain") @NotNull final Domain domain,
            @WebParam(name = "serializer", partName = "serializer") @NotNull final String serializer
    ) throws AccessForbiddenException, IOException {
        if (serviceLocator == null) return;
        serviceLocator.getISessionService().validate(session);
        domain.setUserId(session.getUserId());
        serviceLocator.getIDomainService().export(domain);
        serviceLocator.getIDomainService().serialize(session, domain, serializer);
    }

    @WebMethod
    public void deserialize(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "serializer", partName = "serializer") @NotNull final String deserializer
    ) throws AccessForbiddenException {
        if (serviceLocator == null) return;
        serviceLocator.getISessionService().validate(session);
        serviceLocator.getIDomainService().deserialize(session, deserializer);
    }
}
