package ru.shipova.tm.service;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.endpoint.Session;

@Getter
@Setter
@RequiredArgsConstructor
public class SessionService {
    @Nullable private Session session = null;
}
