package ru.shipova.tm;

import org.jetbrains.annotations.NotNull;
import ru.shipova.tm.bootstrap.Bootstrap;

public final class ClientApplication {
    public static void main(String[] args) {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
    }
}
