package ru.shipova.tm.command.data.json;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.constant.Serializer;
import ru.shipova.tm.constant.TypeOfSerialization;
import ru.shipova.tm.endpoint.AdminUserEndpoint;
import ru.shipova.tm.endpoint.Domain;
import ru.shipova.tm.endpoint.DomainEndpoint;
import ru.shipova.tm.endpoint.Session;

public class DataJsonSaveCommand extends AbstractCommand {

    @Override
    public @Nullable String getName() {
        return "json-save";
    }

    @Override
    public @Nullable String getDescription() {
        return "Save data to JSON.";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator != null) {
            @Nullable final AdminUserEndpoint adminUserEndpoint = serviceLocator.getAdminUserEndpoint();
            if (adminUserEndpoint == null) return;
            @Nullable final DomainEndpoint domainEndpoint = serviceLocator.getDomainEndpoint();
            @Nullable final Session session = serviceLocator.getSessionService().getSession();
            if (!adminUserEndpoint.haveAccessToUsualCommand(session, needAuthorize())) {
                System.out.println("ACCESS DENIED. REQUIRED AUTHORIZATION");
                return;
            }
            if (!adminUserEndpoint.haveAccessToAdminCommand(session, isOnlyAdminCommand())) {
                System.out.println("ACCESS DENIED. NEED ADMINISTRATOR RIGHTS.");
                return;
            }
            if (domainEndpoint == null) return;
            if (session == null) return;
            System.out.println("[DATA JSON SAVE]");
            System.out.println("CHOOSE TYPE OF SERIALIZATION:");
            System.out.println("1. FasterXml;");
            System.out.println("2. Jax-B.");
            @NotNull final String input = serviceLocator.getTerminalService().nextLine();
            @NotNull final String typeOfSerialization = input.toUpperCase();
            @NotNull final Domain domain = new Domain();
            if (typeOfSerialization.equals("1") ||
                    typeOfSerialization.equals(TypeOfSerialization.FASTER_XML.displayName())) {
                @NotNull final String serializer = Serializer.JSON_FASTER_XML.displayName();
                domainEndpoint.serialize(session, domain, serializer);
            }
            if (typeOfSerialization.equals("2") ||
                    typeOfSerialization.equals(TypeOfSerialization.JAX_B.displayName())) {
                @NotNull final String serializer = Serializer.JSON_JAX_B.displayName();
                domainEndpoint.serialize(session, domain, serializer);
            }
            System.out.println("[OK]");
        }
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }

    @Override
    public boolean isOnlyAdminCommand() {
        return true;
    }
}
