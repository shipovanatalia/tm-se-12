package ru.shipova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.endpoint.*;

public final class TaskCreateCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "task-create";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Create new task.";
    }

    @Override
    public void execute() {
        if (serviceLocator != null) {
            @Nullable final AdminUserEndpoint adminUserEndpoint = serviceLocator.getAdminUserEndpoint();
            if (adminUserEndpoint == null) return;
            @Nullable final Session session = serviceLocator.getSessionService().getSession();
            try {
                if (!adminUserEndpoint.haveAccessToUsualCommand(session, needAuthorize())) {
                    System.out.println("ACCESS DENIED. REQUIRED AUTHORIZATION");
                    return;
                }
                System.out.println("[TASK CREATE]");
                @Nullable final TaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
                if (taskEndpoint == null) return;
                System.out.println("ENTER NAME:");
                @NotNull final String taskName = serviceLocator.getTerminalService().nextLine();
                System.out.println("ENTER PROJECT NAME:");
                @NotNull final String projectName = serviceLocator.getTerminalService().nextLine();
                taskEndpoint.createTask(session, taskName, projectName);
                System.out.println("[OK]");
            } catch (AccessForbiddenException_Exception e) {
                System.out.println("ACCESS DENIED.");
            } catch (ProjectDoesNotExistException_Exception e) {
                System.out.println("PROJECT DOES NOT EXISTS");
            }
        }
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }
}
