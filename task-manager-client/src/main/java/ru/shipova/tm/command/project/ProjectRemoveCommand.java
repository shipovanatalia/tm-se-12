package ru.shipova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.endpoint.AccessForbiddenException_Exception;
import ru.shipova.tm.endpoint.AdminUserEndpoint;
import ru.shipova.tm.endpoint.ProjectEndpoint;
import ru.shipova.tm.endpoint.Session;

public final class ProjectRemoveCommand extends AbstractCommand {
    @NotNull
    @Override
    public String getName() {
        return "project-remove";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove selected project.";
    }

    @Override
    public void execute() {
        if (serviceLocator != null) {
            @Nullable final AdminUserEndpoint adminUserEndpoint = serviceLocator.getAdminUserEndpoint();
            if (adminUserEndpoint == null) return;
            try {
                @Nullable final Session session = serviceLocator.getSessionService().getSession();
                if (!adminUserEndpoint.haveAccessToUsualCommand(session, needAuthorize())) {
                    System.out.println("ACCESS DENIED. REQUIRED AUTHORIZATION");
                    return;
                }
                System.out.println("[PROJECT REMOVE]");
                System.out.println("ENTER NAME OF PROJECT:");
                @NotNull final String projectName = serviceLocator.getTerminalService().nextLine();
                @Nullable final ProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
                if (projectEndpoint == null) return;
                projectEndpoint.removeProject(session, projectName);
                System.out.println("[PROJECT " + projectName + " REMOVED]");
            } catch (AccessForbiddenException_Exception e) {
                System.out.println("ACCESS DENIED.");
            }
        }
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }
}
