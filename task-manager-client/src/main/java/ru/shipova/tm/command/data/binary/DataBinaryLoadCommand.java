package ru.shipova.tm.command.data.binary;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.constant.Serializer;
import ru.shipova.tm.endpoint.*;

public class DataBinaryLoadCommand extends AbstractCommand {
    @Override
    @Nullable
    public String getName() {
        return "bin-load";
    }

    @Override
    @Nullable
    public String getDescription() {
        return "Load data from binary file.";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator != null) {
            @Nullable final AdminUserEndpoint adminUserEndpoint = serviceLocator.getAdminUserEndpoint();
            @Nullable final DomainEndpoint domainEndpoint = serviceLocator.getDomainEndpoint();
            @Nullable final Session session = serviceLocator.getSessionService().getSession();
            if (adminUserEndpoint == null) return;
            try {
                if (!adminUserEndpoint.haveAccessToUsualCommand(session, needAuthorize())) {
                    System.out.println("ACCESS DENIED. REQUIRED AUTHORIZATION");
                    return;
                }
                if (!adminUserEndpoint.haveAccessToAdminCommand(session, isOnlyAdminCommand())) {
                    System.out.println("ACCESS DENIED. NEED ADMINISTRATOR RIGHTS.");
                    return;
                }
                System.out.println("[DATA BINARY LOAD]");
                if (domainEndpoint == null) return;
                @NotNull final String deserializer = Serializer.BINARY.displayName();
                domainEndpoint.deserialize(session, deserializer);
            } catch (AccessForbiddenException_Exception e) {
                System.out.println("ACCESS DENIED.");
            }
//            @Nullable final Domain domain =
//                    binarySerializer.deserialize();
//            if (domain == null) return;
//            serviceLocator.getIDomainService().load(domain);
            System.out.println("[OK]");
        }
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }

    @Override
    public boolean isOnlyAdminCommand() {
        return true;
    }
}
