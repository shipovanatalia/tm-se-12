package ru.shipova.tm.command.data.json;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.constant.Serializer;
import ru.shipova.tm.constant.TypeOfSerialization;
import ru.shipova.tm.endpoint.AdminUserEndpoint;
import ru.shipova.tm.endpoint.DomainEndpoint;
import ru.shipova.tm.endpoint.Session;

public class DataJsonLoadCommand extends AbstractCommand {
    @Override
    @NotNull
    public String getName() {
        return "json-load";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Load data from JSON-file.";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator != null) {
            @Nullable final AdminUserEndpoint adminUserEndpoint = serviceLocator.getAdminUserEndpoint();
            @Nullable final DomainEndpoint domainEndpoint = serviceLocator.getDomainEndpoint();
            @Nullable final Session session = serviceLocator.getSessionService().getSession();
            if (adminUserEndpoint == null) return;
            if (!adminUserEndpoint.haveAccessToUsualCommand(session, needAuthorize())) {
                System.out.println("ACCESS DENIED. REQUIRED AUTHORIZATION");
                return;
            }
            if (!adminUserEndpoint.haveAccessToAdminCommand(session, isOnlyAdminCommand())) {
                System.out.println("ACCESS DENIED. NEED ADMINISTRATOR RIGHTS.");
                return;
            }
            if (domainEndpoint == null) return;
            System.out.println("[DATA JSON LOAD]");
            System.out.println("CHOOSE TYPE OF DESERIALIZATION:");
            System.out.println("1. FasterXml;");
            System.out.println("2. Jax-B.");
            @NotNull final String input = serviceLocator.getTerminalService().nextLine();
            @NotNull final String typeOfDeserialization = input.toUpperCase();
            if (typeOfDeserialization.equals("1") ||
                    typeOfDeserialization.equals(TypeOfSerialization.FASTER_XML.displayName())) {
                @NotNull final String deserializer = Serializer.JSON_FASTER_XML.displayName();
                domainEndpoint.deserialize(session, deserializer);
            }
            if (typeOfDeserialization.equals("2") ||
                    typeOfDeserialization.equals(TypeOfSerialization.JAX_B.displayName())) {
                @NotNull final String deserializer = Serializer.JSON_JAX_B.displayName();
                domainEndpoint.deserialize(session, deserializer);
            }
            System.out.println("[OK]");
        }
    }

    @Override
    public boolean needAuthorize() {
        return true;
    }

    @Override
    public boolean isOnlyAdminCommand() {
        return true;
    }
}
