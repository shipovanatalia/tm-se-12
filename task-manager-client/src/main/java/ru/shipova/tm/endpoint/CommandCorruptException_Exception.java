
package ru.shipova.tm.endpoint;

import javax.xml.ws.WebFault;


/**
 * This class was generated by Apache CXF 3.2.7
 * 2019-11-16T14:19:49.714+03:00
 * Generated source version: 3.2.7
 */

@WebFault(name = "CommandCorruptException", targetNamespace = "http://endpoint.tm.shipova.ru/")
public class CommandCorruptException_Exception extends Exception {

    private ru.shipova.tm.endpoint.CommandCorruptException commandCorruptException;

    public CommandCorruptException_Exception() {
        super();
    }

    public CommandCorruptException_Exception(String message) {
        super(message);
    }

    public CommandCorruptException_Exception(String message, java.lang.Throwable cause) {
        super(message, cause);
    }

    public CommandCorruptException_Exception(String message, ru.shipova.tm.endpoint.CommandCorruptException commandCorruptException) {
        super(message);
        this.commandCorruptException = commandCorruptException;
    }

    public CommandCorruptException_Exception(String message, ru.shipova.tm.endpoint.CommandCorruptException commandCorruptException, java.lang.Throwable cause) {
        super(message, cause);
        this.commandCorruptException = commandCorruptException;
    }

    public ru.shipova.tm.endpoint.CommandCorruptException getFaultInfo() {
        return this.commandCorruptException;
    }
}
