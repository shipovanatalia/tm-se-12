package ru.shipova.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.shipova.tm.api.IServiceLocator;
import ru.shipova.tm.command.AbstractCommand;
import ru.shipova.tm.constant.RoleType;
import ru.shipova.tm.endpoint.*;
import ru.shipova.tm.exception.CommandCorruptException;
import ru.shipova.tm.service.SessionService;
import ru.shipova.tm.service.TerminalService;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

/**
 * Класс загрузчика приложения
 */

public final class Bootstrap implements IServiceLocator {
    private static final String NS = "http://endpoint.tm.shipova.ru/";
    @NotNull
    private final TerminalService terminalService = new TerminalService(this);
    @NotNull
    private final SessionService sessionService = new SessionService();
    @Nullable
    private UserEndpoint userEndpoint;
    @Nullable
    private ProjectEndpoint projectEndpoint;
    @Nullable
    private TaskEndpoint taskEndpoint;
    @Nullable
    private SessionEndpoint sessionEndpoint;
    @Nullable
    private DomainEndpoint domainEndpoint;
    @Nullable
    private AdminUserEndpoint adminUserEndpoint;

    //At runtime, find all classes in a Java application that extend a AbstractCommand class
    private final Set<Class<? extends AbstractCommand>> commandSet =
            new Reflections("ru.shipova.tm").getSubTypesOf(AbstractCommand.class);

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    public void init() {
        loadCommands();
        try {
            registryUserEndpoint();
            registryProjectEndpoint();
            registryTaskEndpoint();
            registrySessionEndpoint();
            registryDomainEndpoint();
            registryAdminUserEndpoint();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        System.out.println("*** WELCOME TO TASK MANAGER ***");
        @NotNull String commandName = "";
        while (!"exit".equals(commandName)) {
            commandName = terminalService.nextLine();
            if (!commands.containsKey(commandName)) {
                System.out.println("WRONG COMMAND. ENTER 'help' TO GET ALL AVAILABLE COMMANDS.");
            }
            try {
                execute(commandName);
            } catch (Exception e) {
                System.out.println("CANNOT DO COMMAND.");
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
        }
    }

    private void loadCommands() {
        for (@NotNull final Class commandClass : commandSet) {
            try {
                if (AbstractCommand.class.isAssignableFrom(commandClass))
                    registryCommand((AbstractCommand) commandClass.newInstance());
            } catch (CommandCorruptException | InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    private void registryCommand(@Nullable final AbstractCommand command) throws CommandCorruptException {
        if (command == null) throw new CommandCorruptException();

        @Nullable final String commandName = command.getName(); //Command Line Interface
        @Nullable final String cliDescription = command.getDescription();
        if (commandName == null || commandName.isEmpty()) throw new CommandCorruptException();
        if (cliDescription == null || cliDescription.isEmpty()) throw new CommandCorruptException();

        command.setServiceLocator(this);
        commands.put(commandName, command);
    }

    private void registryUserEndpoint() throws MalformedURLException {
        final URL url = new URL("http://localhost:8180/UserEndpoint?wsdl");
        final QName name = new QName(NS, "UserEndpointService");
        final Service service = Service.create(url, name);
        userEndpoint = service.getPort(UserEndpoint.class);
    }

    private void registryProjectEndpoint() throws MalformedURLException {
        final URL url = new URL("http://localhost:8180/ProjectEndpoint?wsdl");
        final QName name = new QName(NS, "ProjectEndpointService");
        final Service service = Service.create(url, name);
        projectEndpoint = service.getPort(ProjectEndpoint.class);
    }

    private void registryTaskEndpoint() throws MalformedURLException {
        final URL url = new URL("http://localhost:8180/TaskEndpoint?wsdl");
        final QName name = new QName(NS, "TaskEndpointService");
        final Service service = Service.create(url, name);
        taskEndpoint = service.getPort(TaskEndpoint.class);
    }

    private void registrySessionEndpoint() throws MalformedURLException {
        final URL url = new URL("http://localhost:8180/SessionEndpoint?wsdl");
        final QName name = new QName(NS, "SessionEndpointService");
        final Service service = Service.create(url, name);
        sessionEndpoint = service.getPort(SessionEndpoint.class);
    }

    private void registryDomainEndpoint() throws MalformedURLException {
        final URL url = new URL("http://localhost:8180/DomainEndpoint?wsdl");
        final QName name = new QName(NS, "DomainEndpointService");
        final Service service = Service.create(url, name);
        domainEndpoint = service.getPort(DomainEndpoint.class);
    }

    private void registryAdminUserEndpoint() throws MalformedURLException {
        final URL url = new URL("http://localhost:8180/AdminUserEndpoint?wsdl");
        final QName name = new QName(NS, "AdminUserEndpointService");
        final Service service = Service.create(url, name);
        adminUserEndpoint = service.getPort(AdminUserEndpoint.class);
    }

    private void execute(@Nullable final String commandName) throws Exception {
        if (commandName == null || commandName.isEmpty()) return;

        @Nullable final AbstractCommand abstractCommand = commands.get(commandName);
        if (abstractCommand == null) return;
        abstractCommand.execute();
    }

    @NotNull
    public List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

    @Override
    @NotNull
    public TerminalService getTerminalService() {
        return terminalService;
    }

    @Override
    @NotNull
    public SessionService getSessionService() {
        return sessionService;
    }

    @Override
    @Nullable
    public UserEndpoint getUserEndpoint() {
        return userEndpoint;
    }

    @Override
    @Nullable
    public  ProjectEndpoint getProjectEndpoint() {
        return projectEndpoint;
    }

    @Override
    @Nullable
    public TaskEndpoint getTaskEndpoint() {
        return taskEndpoint;
    }

    @Override
    @Nullable
    public SessionEndpoint getSessionEndpoint() {
        return sessionEndpoint;
    }

    @Override
    @Nullable
    public DomainEndpoint getDomainEndpoint() {
        return domainEndpoint;
    }

    @Override
    @Nullable
    public AdminUserEndpoint getAdminUserEndpoint() {
        return adminUserEndpoint;
    }
}
